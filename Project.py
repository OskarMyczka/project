import csv
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QFileDialog
from PyQt5.QtChart import QChart, QChartView, QBarSet, QBarCategoryAxis, QPieSeries, QLineSeries, QBarSeries
from PyQt5.QtCore import *
from PyQt5.QtGui import *


class Window(QMainWindow):

    def __init__(self):
        super(Window, self).__init__()
        self.setGeometry(300, 300, 500, 500)
        self.setWindowTitle("Hello!")
        self.initUI()

    def initUI(self):
        """
        method that creates the buttons
        :return:
        """

        self.button = QtWidgets.QPushButton(self)
        self.button.setText("Bar chart")
        self.button.move(50, 50)
        self.button.clicked.connect(self.create_chartbar)

        self.button2 = QtWidgets.QPushButton(self)
        self.button2.setText("Piechart")
        self.button2.move(200, 50)
        self.button2.clicked.connect(self.create_piechart)

        self.button3 = QtWidgets.QPushButton(self)
        self.button3.setText("Line Chart")
        self.button3.move(350, 50)
        self.button3.clicked.connect(self.create_linechart)

    def create_chartbar(self):
        """
        method that creates the chart bar
        :return:
        """

        filename = QFileDialog.getOpenFileName()
        path = filename[0]
        print(path)

        with open(path, "r") as csvfile:

            readCSV = csv.reader(csvfile, delimiter=',')
            first_column = []
            second_column = []

            for row in readCSV:
                rows = row[1]
                rows2 = row[0]

                first_column.append(rows2)
                second_column.append(rows)

            set0 = QBarSet("colour")

            set0 << int(second_column[0]) << int(second_column[1]) << int(second_column[2]) << int(second_column[3]) << int(
                second_column[4])

            series = QBarSeries()
            series.append(set0)

            chart = QChart()
            chart.addSeries(series)
            chart.setAnimationOptions(QChart.SeriesAnimations)

            axis = QBarCategoryAxis()
            for x in range(len(first_column)):
                axis.append(first_column[x])

            chart.createDefaultAxes()
            chart.setAxisX(axis, series)

            chart.legend().setVisible(True)
            chart.legend().setAlignment(Qt.AlignBottom)

            chartView = QChartView(chart)
            chartView.setRenderHint(QPainter.Antialiasing)

            self.setCentralWidget(chartView)

    def create_piechart(self):
        """
        method that creates the pie chart
        :return:
        """

        filename = QFileDialog.getOpenFileName()
        path = filename[0]
        print(path)

        with open(path, "r") as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            first_column = []
            second_column = []

            for row in readCSV:
                rows = row[1]
                rows2 = row[0]

                first_column.append(rows2)
                second_column.append(rows)

            series = QPieSeries()

            for x in range(len(first_column)):
                series.append(first_column[x], int(second_column[x]))

            chart = QChart()
            chart.addSeries(series)
            chart.setAnimationOptions(QChart.SeriesAnimations)
            chart.setTitle("Pie chart")

            chartview = QChartView(chart)
            chartview.setRenderHint(QPainter.Antialiasing)

        self.setCentralWidget(chartview)

    def create_linechart(self):
        """
        method that creates the line chart
        :return:
        """

        filename = QFileDialog.getOpenFileName()
        path = filename[0]
        print(path)

        with open(path, "r") as csvfile:
            readCSV = csv.reader(csvfile, delimiter=',')
            first_column = []
            second_column = []
            third_column = []
            fourth_column = []

            for row in readCSV:
                rows = row[1]
                rows2 = row[0]
                rows3 = row[3]
                rows4 = row[2]

                first_column.append(rows2)
                second_column.append(rows)
                third_column.append(rows3)
                fourth_column.append(rows4)

            print(first_column)
            series = QLineSeries(self)

            for x in range(len(first_column)):
                series.append(int(first_column[x]), int(second_column[x]))

                series << QPointF(int(third_column[x]), int(fourth_column[x])) << QPointF(int(third_column[x]),
                                                                                    int(fourth_column[x])) << QPointF(
                    int(third_column[x]), int(fourth_column[x]), ) << QPointF(int(third_column[x]), int(fourth_column[x]))

            chart = QChart()

            chart.addSeries(series)
            chart.createDefaultAxes()
            chart.setAnimationOptions(QChart.SeriesAnimations)
            chart.setTitle("Line Chart Example")

            chart.legend().setVisible(True)
            chart.legend().setAlignment(Qt.AlignBottom)

            chartview = QChartView(chart)
            chartview.setRenderHint(QPainter.Antialiasing)

            self.setCentralWidget(chartview)


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Window()
    ui.show()
    sys.exit(app.exec_())
